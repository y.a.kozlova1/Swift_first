import Foundation

/*
 возвращаемым значением функции makeBuffer является функция buffer, которая принимает в качестве аргумента String и ничего не возвращает (Void)
 */
func makeBuffer() -> (String) -> () {
    var storage = "" // инициализируем наше хранилище
    
    func buffer(_ value: String) {
        /*
         если строка value не пустая, то пополняем ею хранилище storage,
         если пустая, то выводим то, что есть в хранилище
         */
        !value.isEmpty ? storage += value : print(storage)
    }
    
    return buffer
}

var buffer = makeBuffer()

/*
 // для тестирования
 buffer("")
 buffer("Value changed and saved in storage")
 buffer("")
 buffer(" again")
 buffer("")
 */

