class NameList {
    var nameList: [String] = []
    
    func addName(name: String) {
        nameList.append(name)
    }
    
    func printNames() {
        var chars: [Character] = []
        
        for name in nameList {
            let nameFirstChar = name[name.startIndex]
            if !chars.contains(nameFirstChar) {
                chars.append(nameFirstChar)

            }
        }
        
        for char in chars.sorted() {
            print("\(char)")
            
            for name in nameList {
                if name[name.startIndex] == char {
                    print(name)
                }
            }
            print()
        }
    }
}

let list = NameList()
list.addName(name: "Акулова")
list.addName(name: "Валерьянов")
list.addName(name: "Бочкин")
list.addName(name: "Бабочкина")
list.addName(name: "Арбузов")
list.addName(name: "Васильева")
list.printNames()
