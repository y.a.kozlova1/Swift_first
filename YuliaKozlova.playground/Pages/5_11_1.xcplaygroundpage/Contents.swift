import Foundation

/*
 по заданию нужно создать открытый класс, доступный для наследования, но нет уточнения, какой предел видимости класса должен быть. По умолчанию модификатор доступа у класс - internal, кажется, что для нашей задачи его должно было хватить, но ставлю open из-за формулировки
 */
open class Shape {
    open func calculateArea() -> Double {
        fatalError("not implemented")
    }
    open func calculatePerimeter() -> Double {
        fatalError("not implemented")
    }
}

final class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }

    override func calculateArea() -> Double {
        height * width
    }
    
    override func calculatePerimeter() -> Double {
        2 * (height + width)
    }
}

final class Circle: Shape {
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        Double.pi * pow(radius, 2)
    }
    
    override func calculatePerimeter() -> Double {
        2 * Double.pi * radius
    }
}

final class Square: Shape {
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        pow(side, 2)
    }
    
    override func calculatePerimeter() -> Double {
        4 * side
    }
}

var shapes: [Shape] = [Circle(radius: 10), Square(side: 5), Rectangle(height: 10, width: 5)]

/*
 Чтобы защитить себя от вводных значений <= 0, можно добавить функцию, которая будет возвращать булевое значение. В случае валидных данных выводить значения, в случае false завершать работу или выводить ошибку. Или какой-то аналог try catch в swift.. Но по заданию это вроде не требуется.
 */

// еще по заданию не совсем понятно, нужно ли считать параметры каждой фигуры или общее значение, оставлю два решения
var area: Double = 0
var perimeter: Double = 0

for shape in shapes {
    shape.calculateArea()
    shape.calculatePerimeter()
    
    area += shape.calculateArea()
    perimeter += shape.calculatePerimeter()
}

print("Площадь: \(area), периметр: \(perimeter)")

/*
 var area: Double
 var perimeter: Double
 
 for shape in shapes {
    area = shape.calculateArea()
    perimeter = shape.calculatePerimeter()
    
    print("Фигура - \(type(of: shape)): площадь: \(area), периметр: \(perimeter)")
}
*/
