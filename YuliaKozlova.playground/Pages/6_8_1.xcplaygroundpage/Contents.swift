struct Candidate {
    enum Grade {
        case junior
        case middle
        case senior
    }
     
    /// Грейд (уровень) кандидата
    let grade: Grade
    /// Требуемая зарплата
    let requiredSalary: Int
    /// Полное имя кандидата
    let fullName: String
}

protocol CandidateFilter {
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate]
}

final class GradeFilter: CandidateFilter {
    let expectedGrade: Candidate.Grade
    
    init(expectedGrade: Candidate.Grade) {
        self.expectedGrade = expectedGrade
    }
    
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        candidates.filter { $0.grade == expectedGrade }
    }
}

final class SalaryFilter: CandidateFilter {
    let expectedSalary: Int
    
    init(expectedSalary: Int) {
        self.expectedSalary = expectedSalary
    }
    
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        candidates
            .filter { $0.requiredSalary <= expectedSalary }
            .sorted { $0.requiredSalary < $1.requiredSalary }
    }
}

final class NameFilter: CandidateFilter {
    let searchString: String
    
    init(searchString: String) {
        self.searchString = searchString
    }
    
    func filterCandidates(_ candidates: [Candidate]) -> [Candidate] {
        candidates.filter { $0.fullName.lowercased().contains(searchString.lowercased()) }
    }
}

let candidates: [Candidate] = [
    Candidate(grade: .junior, requiredSalary: 10000, fullName: "Ivan Ivanov"),
    Candidate(grade: .senior, requiredSalary: 50000, fullName: "Maria Sidorova"),
    Candidate(grade: .middle, requiredSalary: 35000, fullName: "Sarah Connor"),
    Candidate(grade: .middle, requiredSalary: 35200, fullName: "Alexander Ivanov"),
    Candidate(grade: .junior, requiredSalary: 5500, fullName: "Maria Pushkina"),
    Candidate(grade: .senior, requiredSalary: 57000, fullName: "Nif Nif")
]

func printFilteredCandidates(_ candidates: [Candidate]) {
    candidates.forEach { candidate in
        print(
            "Name: \(candidate.fullName), Grade: \(candidate.grade), Salary: \(candidate.requiredSalary)"
        )
    }
}

let filteredByGrade = GradeFilter(expectedGrade: .middle)
let filteredBySalary = SalaryFilter(expectedSalary: 35200)
let filteredByName = NameFilter(searchString: "maria")

printFilteredCandidates(filteredByGrade.filterCandidates(candidates))
print("---")
printFilteredCandidates(filteredBySalary.filterCandidates(candidates))
print("---")
printFilteredCandidates(filteredByName.filterCandidates(candidates))
