import CoreGraphics

//Выделила размеры экранов в отдельную структуру, так как структура Device с размерами казалась перегруженной информацией
struct ScreenSize {
    let width: CGFloat
    let height: CGFloat
    
    static let iPhone14Pro = ScreenSize(width: 393, height: 852)
    static let iPhoneXR = ScreenSize(width: 414, height: 896)
    static let iPadPro = ScreenSize(width: 834, height: 1194)
}

struct Device {
    let name: String
    let screenSize: ScreenSize //изменила тип из-за добавления структуры
    let screenDiagonal: Double
    let scaleFactor: Int
    
    static let iPhone14Pro = Device(
        name: "iPhone 14 Pro",
        screenSize: ScreenSize.iPhone14Pro,
        screenDiagonal: 6.1,
        scaleFactor: 3
    )
    static let iPhoneXR = Device(
        name: "iPhone XR",
        screenSize: ScreenSize.iPhoneXR,
        screenDiagonal: 6.06,
        scaleFactor: 2
    )
    static let iPadPro = Device(
        name: "iPad Pro",
        screenSize: ScreenSize.iPadPro,
        screenDiagonal: 11,
        scaleFactor: 2
    )
    
    func physicalSize() -> CGSize {
        return CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}

let deviceSize = Device.iPhoneXR.physicalSize()
print("Физический размер выбранного девайса: \(deviceSize)")

/*
enum Device {
    case iPhone14Pro
    case iPhoneXR
    case iPadPro
     
    var name: String {
        switch self {
        case .iPhone14Pro: return "iPhone 14 Pro"
        case .iPhoneXR: return "iPhone XR"
        case .iPadPro: return "iPad Pro"
        }
    }
     
    var screenSize: CGSize {
        switch self {
        case .iPhone14Pro: return CGSize(width: 393, height: 852)
        case .iPhoneXR: return CGSize(width: 414, height: 896)
        case .iPadPro: return CGSize(width: 834, height: 1194)
        }
    }
     
    var screenDiagonal: Double {
        switch self {
        case .iPhone14Pro: return 6.1
        case .iPhoneXR: return 6.06
        case .iPadPro: return 11
        }
    }
     
    var scaleFactor: Int {
        switch self {
        case .iPhone14Pro:
            return 3
        case .iPhoneXR, .iPadPro:
            return 2
        }
    }
     
    func physicalSize() -> CGSize {
        return CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}

let deviceSize = Device.iPhoneXR.physicalSize()

print(deviceSize)
*/
