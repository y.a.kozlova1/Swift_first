import Foundation

func checkPrimeNumber (_ number: Int) -> Bool {
    
    //Любое число меньше 2 не является простым. Оберегаем себя от ошибок компиляции при подаче на вход таких значений
    if number < 2 {
        return false
    }
    
    /*
     Проверяем, делится ли без остатка наше число на всё, кроме 1 и самого себя.
     Вообще я подумала, что для делителя можно было бы отбросить все четные числа, но я сходу не сообразила, как это просто сделать. Если в функцию передать большое число, то будет много итераций проверок деления без остатка и кажется в случае с таким циклом мы потратим овердохрена времени на вычисления =/
     */
    for n in 2..<number {
        if number % n == 0 {
            return false
        }
    }
    return true
}

/*
 // для тестирования
 print(checkPrimeNumber(6)) // false
 print(checkPrimeNumber(31)) // true
 print(checkPrimeNumber(1)) // false
 print(checkPrimeNumber(2)) // true
 print(checkPrimeNumber(0)) // false
 print(checkPrimeNumber(-1)) // false
 */


