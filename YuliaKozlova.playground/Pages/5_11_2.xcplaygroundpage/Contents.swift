func findIndexWithGenerics<T: Equatable>(of valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndexWithGenerics = findIndexWithGenerics(of: "лама", in: arrayOfString) {
    print("Индекс ламы: \(foundIndexWithGenerics)")
}

let arrayOfDouble = [0.0, 1.32, -2.5, 66.999, 123.1]
if let foundIndexWithGenerics = findIndexWithGenerics(of: 0.0, in: arrayOfDouble) {
    print("Индекс 0.0: \(foundIndexWithGenerics)")
}

let arrayOfInt = [-2, 123, 0, 67, 5]
if let foundIndexWithGenerics = findIndexWithGenerics(of: 5, in: arrayOfInt) {
    print("Индекс 5: \(foundIndexWithGenerics)")
}
